extends Node

const scene_dir = "res://scenes/"

var levels = []
var lvl_id
var curr_lvl_ref = null

var last_id = null

func _ready():
	levels.push_back(ResourceLoader.load(scene_dir + "lvl_menu.tscn"))
	levels.push_back(ResourceLoader.load(scene_dir + "lvl_1.tscn"))
	levels.push_back(ResourceLoader.load(scene_dir + "lvl_2.tscn"))
	levels.push_back(ResourceLoader.load(scene_dir + "lvl_3.tscn"))
	levels.push_back(ResourceLoader.load(scene_dir + "lvl_4.tscn"))
	levels.push_back(ResourceLoader.load(scene_dir + "lvl_5.tscn"))
	levels.push_back(ResourceLoader.load(scene_dir + "lvl_6.tscn"))
	levels.push_back(ResourceLoader.load(scene_dir + "lvl_7.tscn"))
	levels.push_back(ResourceLoader.load(scene_dir + "win.tscn"))
	levels.push_back(ResourceLoader.load(scene_dir + "lose.tscn"))
	
	curr_lvl_ref = get_tree().get_root().get_child(get_tree().get_root().get_child_count() - 1)

func load_level(path):
	get_tree().set_pause(false)
	
	Globals.set("LEVEL CLEAR", false)
	call_deferred("_deferred_load_level", path)
func _deferred_load_level(path):
	curr_lvl_ref.free()
	
	var obj = ResourceLoader.load(scene_dir + path)
	
	curr_lvl_ref = obj.instance()
	
	get_tree().get_root().add_child(curr_lvl_ref)

func switch_level(i):
	Globals.set("LEVEL_CLEAR", false)
	call_deferred("_deferred_switch_level", i)
func _deferred_switch_level(i):
	last_id = lvl_id
	lvl_id = i
	
	curr_lvl_ref.free()
	
	curr_lvl_ref = levels[i].instance()
	
	get_tree().get_root().add_child(curr_lvl_ref)

func next_level():
	lvl_id += 1
	
	lvl_id %= levels.size()
	
	switch_level(lvl_id)

func last_level():
	if(last_id != null):
		last_id %= levels.size()
		
		switch_level(last_id)