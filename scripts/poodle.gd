extends Sprite

const borders = Rect2(0, 82, 256, 142)

export(float) var spawn_t = 0.0
export(float) var spawn_wait = 4.0
#export(Vector2) var spawn_pos
var spawn_pos = Vector2()

#const dying_dur = 1.2

const lin_speed = 240
const hit_speed = 60
const min_dist = 100
const max_dist = 120
var velocity = Vector2(0, 0)

const bark_speed = 130
const bark_life = 2.0
const bark_period = 0.6

const def_health = 1
var health

#const recover_dur = 0.5
var recover_t = 0.0

const max_attack_off = 20
var attack_t = 0.0

#const stance_dur = 0.6

enum state {WALK, ATTACK, HIT, STANCE, DYING, DEAD}
var curr_state

var player

func _ready():
	set_fixed_process(true)
	set_process(true)
	
	spawn_pos = self.get_global_pos()
	curr_state = DEAD

func contain_within_borders():
	var offset_t = self.get_offset().y
	var offset_b = self.get_offset().y + self.get_offset().height
	var offset_l = self.get_offset().x
	var offset_r = self.get_offset().x + self.get_offset().width
	
	if(self.get_global_pos().x + offset_l < borders.pos.x):
		self.set_global_pos(Vector2(borders.pos.x - offset_l, self.get_global_pos().y))
	if(self.get_global_pos().x + offset_r > borders.end.x):
		self.set_global_pos(Vector2(borders.end.x - offset_r, self.get_global_pos().y))
	if(self.get_global_pos().y + offset_t < borders.pos.y):
		self.set_global_pos(Vector2(self.get_global_pos().x, borders.pos.y - offset_t))
	if(self.get_global_pos().y + offset_b > borders.end.y):
		self.set_global_pos(Vector2(self.get_global_pos().x, borders.end.y - offset_b))

func reset():
	self.set_global_pos(spawn_pos)
	health = def_health
	spawn_t = 0.0
	attack_t = 0.0
	velocity = Vector2(0, 0)
	curr_state = state.WALK
	self.get_node("AnimationPlayer").play("walk")
	set_hidden(false)

func _process(delta):
	if(curr_state == state.HIT or curr_state == state.DYING):
		if(self.get_node("AnimationPlayer").get_current_animation() != "hit"):
			self.get_node("AnimationPlayer").stop()
			self.get_node("AnimationPlayer").play("hit")
	
	elif(curr_state == state.ATTACK):
		if(self.get_node("AnimationPlayer").get_current_animation() != "attack"):
			self.get_node("AnimationPlayer").stop()
			self.get_node("AnimationPlayer").play("attack")
	
	elif(curr_state == state.WALK):
		if(self.get_node("AnimationPlayer").get_current_animation() != "walk"):
			self.get_node("AnimationPlayer").stop()
			self.get_node("AnimationPlayer").play("walk")
			
	elif(curr_state == state.STANCE):
		if(self.get_node("AnimationPlayer").get_current_animation() != "stance"):
			self.get_node("AnimationPlayer").stop()
			self.get_node("AnimationPlayer").play("stance")
	
func _fixed_process(delta):
	print(curr_state)
	
	if(curr_state == state.DEAD):
		spawn_t += delta
		if(spawn_t >= spawn_wait and not Globals.get("LEVEL_CLEAR")):
			reset()
	elif(curr_state == state.DYING):
		spawn_t += delta
		
		self.set_hidden(int(spawn_t * 10 / 3 + 1) % 2)
		
		if(spawn_t >= self.get_node("AnimationPlayer").get_current_animation_length()):
			spawn_t = 0.0
			
			self.set_pos(spawn_pos)
			self.set_hidden(true)
			curr_state = state.DEAD
	else:
		if(curr_state == state.HIT):
			recover_t += delta
			if(recover_t >= self.get_node("AnimationPlayer").get_current_animation_length()):
				recover_t = 0.0
				curr_state = state.WALK
			
			self.set_global_pos(self.get_global_pos() + velocity * delta)
		
		elif(curr_state == state.STANCE):
			attack_t += delta
			if(attack_t >= self.get_node("AnimationPlayer").get_current_animation_length()):
				attack_t = 0.0
				var dir = get_node("../Player").get_global_pos() - self.get_global_pos()
				self.get_node("Bark1").shoot(dir.normalized() * bark_speed, bark_life)
				curr_state = state.ATTACK
		
		elif(curr_state == state.ATTACK):
			#Bark at the player
			attack_t += delta
			
			if(attack_t >= 2 * bark_period):
				attack_t = 0.0
				self.get_node("Bark1").stop()
				self.get_node("Bark2").stop()
				curr_state = state.WALK
			elif(attack_t >= bark_period):
				var dir = get_node("../Player").get_global_pos() - self.get_global_pos()
				self.get_node("Bark2").shoot(dir.normalized() * bark_speed, bark_life)
		
		else:
			var dir = self.get_global_pos() - get_node("../Player").get_global_pos()
			
			if(dir.length() < min_dist):
				curr_state = state.WALK
				velocity = dir.normalized() * lin_speed
				
				self.set_global_pos(self.get_global_pos() + velocity * delta)
				contain_within_borders()
			elif(dir.length() > max_dist):
				curr_state = state.WALK
				velocity = -dir.normalized() * lin_speed
				
				self.set_global_pos(self.get_global_pos() + velocity * delta)
				contain_within_borders()
			
			elif(curr_state != state.ATTACK):
				curr_state = state.STANCE
				velocity = -dir.normalized() * lin_speed
			
			if(velocity.x < 0):
				self.set_scale(Vector2(-1, 1))
			else:
				self.set_scale(Vector2(1, 1))

func hit(attack_dir):
	if(curr_state != state.HIT and curr_state != state.DEAD and curr_state != state.DYING):
		self.get_node("SamplePlayer").play("hit_2")
		health -= 1
		if(health <= 0):
			curr_state = state.DYING
		else:
			curr_state = state.HIT
			attack_t = 0.0
			
			var dir = self.get_global_pos() - attack_dir
			velocity = dir.normalized() * hit_speed

func on_bark_hit(body):
	if(curr_state != state.DEAD and curr_state != state.DYING and curr_state != state.HIT):
		body.get_parent().hit(self.get_global_pos())

func on_enemy_exit(body):
	#print("player exit")
	player = null