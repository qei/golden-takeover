extends Area2D

func _ready():
	self.connect("body_enter", self.get_parent(), "on_enemy_enter")
	self.connect("body_exit", self.get_parent(), "on_enemy_exit")