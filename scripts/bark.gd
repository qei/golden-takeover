extends Area2D

var velocity = null
var lifetime = null
var life_t = 0.0

func shoot(p_vel, p_lt):
	if(velocity == null):
		self.get_node("SamplePlayer").play("bark_1")
		self.set_hidden(false)
		self.set_enable_monitoring(true)
		velocity = p_vel
		lifetime = p_lt
		set_fixed_process(true)

func stop():
	self.get_node("SamplePlayer").stop_all()
	self.set_hidden(true)
	self.set_enable_monitoring(false)
	velocity = null
	lifetime = null
	life_t = 0.0
	self.set_pos(Vector2(0, 0))
	set_fixed_process(false)

func _ready():
	self.connect("body_enter", self.get_parent(), "on_bark_hit")
	self.connect("body_enter", self, "stop")
	self.connect("body_exit", self.get_parent(), "on_enemy_exit")
	
	self.stop()

func _fixed_process(delta):
	if(lifetime != null):
		life_t += delta
		if(life_t >= lifetime):
			self.stop()
	
	if(velocity != null):
		self.set_global_pos(self.get_global_pos() + velocity * delta)