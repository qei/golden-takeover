extends Sprite

const bark_spots = [Vector2(190, 100), Vector2(190, 145), Vector2(190, 190)]
var bark_id = 0
const bark_speed = 250
const bark_life = 1
const bark_period = 0.4

export(float) var spawn_t = 0.0
export(float) var spawn_wait = 4.0
#export(Vector2) var spawn_pos
var spawn_pos = Vector2()

#const dying_dur = 0.8

const lin_speed = 140
const hit_speed = 10
const target_dist = 10
var velocity = Vector2(0, 0)

const def_health = 10
var health

#const recover_dur = 0.5
var recover_t = 0.0

#const attack_dur = 0.3
const attack_cool_off = 0.5
var attack_t = 0.0
var attack_landed = false

#const stance_dur = 0.2

enum state {IDLE, ATTACK, HIT, STANCE, DYING, DEAD, RIPOSTE, WALK, BARK, INHALE}
var curr_state

enum stage {NORMAL, BARKS, MINIONS}
var curr_stage

var player

func _ready():
	self.get_node("../ObjectiveLabel").add_to_max()
	
	set_process(true)
	set_fixed_process(true)
	
	spawn_pos = self.get_global_pos()
	reset()

func reset():
	self.set_global_pos(spawn_pos)
	health = def_health
	#self.get_node("../BossHealth").update_health(health, def_health)
	spawn_t = 0.0
	attack_t = 0.0
	attack_landed = false
	velocity = Vector2(0, 0)
	curr_state = state.IDLE
	self.get_node("AnimationPlayer").play("walk")
	set_hidden(false)

func _process(delta):
	if(curr_state == state.HIT):
		if(self.get_node("AnimationPlayer").get_current_animation() != "hit"):
			self.get_node("AnimationPlayer").stop()
			self.get_node("AnimationPlayer").play("hit")
	
	elif(curr_state == state.DYING):
		if(self.get_node("AnimationPlayer").get_current_animation() != "dying"):
			self.get_node("AnimationPlayer").stop()
			self.get_node("AnimationPlayer").play("dying")
	
	elif(curr_state == state.ATTACK):
		if(self.get_node("AnimationPlayer").get_current_animation() != "attack"):
			self.get_node("AnimationPlayer").stop()
			self.get_node("AnimationPlayer").play("attack")
	
	elif(curr_state == state.IDLE):
		if(self.get_node("AnimationPlayer").get_current_animation() != "idle"):
			self.get_node("AnimationPlayer").stop()
			self.get_node("AnimationPlayer").play("idle")
			
	elif(curr_state == state.STANCE):
		if(self.get_node("AnimationPlayer").get_current_animation() != "stance"):
			self.get_node("AnimationPlayer").stop()
			self.get_node("AnimationPlayer").play("stance")
			
	elif(curr_state == state.RIPOSTE):
		if(self.get_node("AnimationPlayer").get_current_animation() != "riposte"):
			self.get_node("AnimationPlayer").stop()
			self.get_node("AnimationPlayer").play("riposte")
			
	elif(curr_state == state.BARK):
		if(self.get_node("AnimationPlayer").get_current_animation() != "bark"):
			self.get_node("AnimationPlayer").stop()
			self.get_node("AnimationPlayer").play("bark")
			
	elif(curr_state == state.WALK):
		if(self.get_node("AnimationPlayer").get_current_animation() != "walk"):
			self.get_node("AnimationPlayer").stop()
			self.get_node("AnimationPlayer").play("walk")
			
	elif(curr_state == state.INHALE):
		if(self.get_node("AnimationPlayer").get_current_animation() != "inhale"):
			self.get_node("AnimationPlayer").stop()
			self.get_node("AnimationPlayer").play("inhale")
	
func _fixed_process(delta):
	if(curr_state == state.DEAD):
		pass
	elif(curr_state == state.DYING):
		spawn_t += delta
		
		if(spawn_t >= self.get_node("AnimationPlayer").get_current_animation_length()):
			spawn_t = 0.0
#			self.get_node("EventPlayer").play()
			curr_state = state.DEAD
	else:
		if(curr_state == state.RIPOSTE):
			attack_t += delta
			if(attack_t >= self.get_node("AnimationPlayer").get_current_animation_length()):
				attack_t = 0.0
				curr_state = state.WALK
		
		else:			
			if(curr_state == state.HIT):
				recover_t += delta
				if(recover_t >= self.get_node("AnimationPlayer").get_current_animation_length()):
					recover_t = 0.0
					if(player != null):
						attack_t = 0.0
						curr_state = state.ATTACK
					else:
						curr_state = state.WALK
				
				self.set_global_pos(self.get_global_pos() + velocity * delta)
			
			elif(curr_state == state.STANCE):
				attack_t += delta
				if(attack_t >= self.get_node("AnimationPlayer").get_current_animation_length()):
					attack_t = 0.0
					curr_state = state.ATTACK
				if(player == null):
					attack_t = 0.0
					curr_state = state.WALK
					attack_landed = false
			
			elif(curr_state == state.ATTACK):
				attack_t += delta
				if(attack_t >= self.get_node("AnimationPlayer").get_current_animation_length()):
					curr_state = state.WALK
					attack_landed = false
				
				if(player != null and attack_t < self.get_node("AnimationPlayer").get_current_animation_length() and not attack_landed):
					player.get_parent().hit(self.get_global_pos())
					attack_landed = true
			
			elif(curr_state == state.WALK):
				bark_id %= 3
				
				var dir = bark_spots[bark_id] - self.get_global_pos()
				velocity = dir.normalized() * lin_speed
				self.set_global_pos(self.get_global_pos() + velocity * delta)
			
				if((self.get_global_pos() - bark_spots[bark_id]).length() < 2):
					bark_id += 1
					attack_t = 0.0
					curr_state = state.INHALE
			
			elif(curr_state == state.INHALE):
				attack_t += delta
				if(attack_t >= self.get_node("AnimationPlayer").get_current_animation_length()):
					attack_t = 0.0
					self.get_node("Bark1").shoot(Vector2(-1, 0) * bark_speed, bark_life)
					curr_state = state.BARK
		
			elif(curr_state == state.BARK):
				#Bark at the player
				attack_t += delta
				
				if(attack_t >= 3 * bark_period):
					attack_t = 0.0
					self.get_node("Bark1").stop()
					self.get_node("Bark2").stop()
					self.get_node("Bark3").stop()
					curr_state = state.WALK
				elif(attack_t >= 2 * bark_period):
					self.get_node("Bark3").shoot(Vector2(-1, 0) * bark_speed, bark_life)
				elif(attack_t >= bark_period):
					self.get_node("Bark2").shoot(Vector2(-1, 0) * bark_speed, bark_life)
			

func hit(attack_dir):
	if(curr_state != state.HIT and curr_state != state.DEAD and curr_state != state.DYING):
		if(curr_state == state.STANCE or curr_state == state.INHALE):
			self.get_node("SamplePlayer").play("hit_2")
			health -= 1
			get_node("../BossHealth").update_health(health, def_health)
			print(health)
			#get_node("../BossHealth").update_health(health, def_health)
			if(health <= 0):
				self.get_node("../ObjectiveLabel").add_to_objectives()
				spawn_t = 0.0
				curr_state = state.DYING
			else:
				curr_state = state.HIT
				attack_t = 0.0
				attack_landed = false
				
				var dir = self.get_global_pos() - attack_dir
				velocity = dir.normalized() * hit_speed
		else:
			get_node("../Player").hit(self.get_global_pos())
			curr_state = state.RIPOSTE

func on_bark_hit(body):
	if(curr_state != state.DEAD and curr_state != state.DYING and curr_state != state.HIT):
		body.get_parent().hit(self.get_global_pos())

func on_enemy_enter(body):
	#print("player enter")
	player = body
	if(curr_state == state.IDLE):
		curr_state = state.STANCE

func on_enemy_exit(body):
	#print("player exit")
	player = null