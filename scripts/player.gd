extends Sprite

const borders = Rect2(0, 82, 256, 142)

const lin_speed = 180
const hit_speed = 60

var velocity = Vector2(0, 0)
var pee_slow_down = 0.3

export(int) var def_health = 8
var health

#const recover_dur = 0.2
var recover_t = 0.0

#const attack_dur = 0.4
const attack_cool_off = 0.8
var attack_t = 0.0

enum state {IDLE, WALK, PEE, ATTACK, HIT, DEAD}
var curr_state = state.IDLE

var enemies_in_range = []

var ATTCK_DWN = false

const death_dur = 1.5

func _ready():
	set_process(true)
	set_fixed_process(true)
	set_process_input(true)
	
	self.get_node("AnimationPlayer").play("idle")
	
	restart()

func restart():
	health = def_health
	self.set_global_pos(Vector2(0, self.get_global_pos().y))
	get_node("../PlayerHealth").set_text(str(health))
	get_node("../PlayerHealth").update()

func contain_within_borders():
	var offset_t = self.get_offset().y
	var offset_b = self.get_offset().y + self.get_offset().height
	var offset_l = self.get_offset().x
	var offset_r = self.get_offset().x + self.get_offset().width
	
	if(self.get_global_pos().x + offset_l < borders.pos.x):
		self.set_global_pos(Vector2(borders.pos.x - offset_l, self.get_global_pos().y))
	if(self.get_global_pos().x + offset_r > borders.end.x):
		if(Globals.get("LEVEL_CLEAR")):
			restart()
			get_tree().set_pause(false)
			get_node("/root/globals").next_level()
		else:
			self.set_global_pos(Vector2(borders.end.x - offset_r, self.get_global_pos().y))
	if(self.get_global_pos().y + offset_t < borders.pos.y):
		self.set_global_pos(Vector2(self.get_global_pos().x, borders.pos.y - offset_t))
	if(self.get_global_pos().y + offset_b > borders.end.y):
		self.set_global_pos(Vector2(self.get_global_pos().x, borders.end.y - offset_b))

func on_timeout():
	attack_t = 0.0
	get_node("../Timer/Label").add_color_override("font_color", Color("B21030"))
	get_tree().set_pause(true)
	curr_state = state.DEAD

func _process(delta):	
	if(curr_state == state.HIT or curr_state == state.DEAD):
		if(self.get_node("AnimationPlayer").get_current_animation() != "hit"):
			self.get_node("AnimationPlayer").stop()
			self.get_node("AnimationPlayer").play("hit")
	
	elif(curr_state == state.PEE):
		if(self.get_node("AnimationPlayer").get_current_animation() != "pee"):
			self.get_node("AnimationPlayer").stop()
			self.get_node("AnimationPlayer").play("pee")
	
	elif(curr_state == state.ATTACK):
		if(self.get_node("AnimationPlayer").get_current_animation() != "attack"):
			self.get_node("AnimationPlayer").stop()
			self.get_node("AnimationPlayer").play("attack")
	
	elif(curr_state == state.WALK):
		if(self.get_node("AnimationPlayer").get_current_animation() != "walk"):
			self.get_node("AnimationPlayer").stop()
			self.get_node("AnimationPlayer").play("walk")
	
	elif(curr_state == state.IDLE):
		if(self.get_node("AnimationPlayer").get_current_animation() != "idle"):
			self.get_node("AnimationPlayer").stop()
			self.get_node("AnimationPlayer").play("idle")

func _fixed_process(delta):
	if(curr_state == state.DEAD):
		attack_t += delta
		if(attack_t >= death_dur):
			get_tree().set_pause(false)
			get_node("/root/globals").switch_level(9)
	else:
		if(curr_state == state.ATTACK or attack_t > 0.0):
			attack_t += delta
			if(attack_t >= attack_cool_off):
				attack_t = 0.0
		
		if(curr_state == state.HIT):
			recover_t += delta
			if(recover_t >= self.get_node("AnimationPlayer").get_current_animation_length()):
				recover_t = 0.0
				curr_state = state.IDLE
			
			self.set_global_pos(self.get_global_pos() + velocity * delta)
			contain_within_borders()
		
		elif(curr_state == state.ATTACK):
			if(attack_t >= self.get_node("AnimationPlayer").get_current_animation_length()):
				curr_state = state.WALK
			
			for enemy in self.get_node("Attack").get_overlapping_bodies():
				if(attack_t < self.get_node("AnimationPlayer").get_current_animation_length()):
					enemy.get_parent().hit(self.get_global_pos())
		
		else:
			if(Input.is_action_pressed("ATTACK")):
				if(not ATTCK_DWN):
					ATTCK_DWN = true
					curr_state = state.ATTACK
			else:
				if(ATTCK_DWN):
					ATTCK_DWN = false
			
				if(Input.is_action_pressed("PEE")):
					curr_state = state.PEE
				else:
					curr_state = state.IDLE
				
				if(Input.is_action_pressed("MOVE_LEFT")):
					velocity.x = -lin_speed
					self.set_scale(Vector2(-1, 1))
				elif(Input.is_action_pressed("MOVE_RIGHT")):
					velocity.x =  lin_speed
					self.set_scale(Vector2(1, 1))
				else:
					velocity.x = 0
				
				if(Input.is_action_pressed("MOVE_UP")):
					velocity.y = -lin_speed
				elif(Input.is_action_pressed("MOVE_DOWN")):
					velocity.y =  lin_speed
				else:
					velocity.y = 0
				
				if(velocity != Vector2(0, 0)):
					if(curr_state != state.PEE):
						curr_state = state.WALK
					else:
						velocity *= pee_slow_down
					
					self.set_global_pos(self.get_global_pos() + velocity * delta)
					contain_within_borders()
				else:
					curr_state = state.IDLE

func hit(attack_dir):
	if(curr_state != state.HIT and curr_state != state.DEAD):
		self.get_node("SamplePlayer").play("hit_1")
		health -= 1
		if(health <= 0):
			attack_t = 0.0
			get_node("../PlayerHealth").add_color_override("font_color", Color("B21030"))
			get_tree().set_pause(true)
			curr_state = state.DEAD
		
		else:
			curr_state = state.HIT
			
			var dir = self.get_global_pos() - attack_dir
			velocity = dir.normalized() * hit_speed
		
		get_node("../PlayerHealth").set_text(str(health))
		get_node("../PlayerHealth").update()

func on_enemy_enter(body):
	pass
	#print("enemy enter")
	#enemies_in_range.push_back(body)

func on_enemy_exit(body):
	pass
	#print("enemy exit")
	#enemies_in_range.remove(body)