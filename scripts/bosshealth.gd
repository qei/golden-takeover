extends Node2D

const hbar_rect = Rect2(0, 216, 256, 8)
export(Color) var hbar_bgc = Color("000000")
export(Color) var hbar_fgc = Color("EBD320")

var hbar_percent = 1.0

func update_health(p_curr, p_max):
	hbar_percent = float(p_curr) / float(p_max)
	print(hbar_percent)
	update()

func _ready():
	update()

func _draw():
	draw_rect(hbar_rect, hbar_bgc)
	draw_rect(Rect2(hbar_rect.pos.x, hbar_rect.pos.y, hbar_rect.size.x * hbar_percent, hbar_rect.size.y), hbar_fgc)