extends Label

var obj_done = 0
var obj_max = 0

func _ready():
	self.set_text(str(obj_done) + "/" + str(obj_max))
	self.update()

func add_to_max():
	obj_max += 1
	
	self.set_text(str(obj_done) + "/" + str(obj_max))
	self.update()

func add_to_objectives():
	obj_done += 1
	if(obj_done >= obj_max):
		self.add_color_override("font_color", Color("00FF00"))
		get_node("../Timer").stop()
		get_node("../Timer/Label").add_color_override("font_color", Color("00FF00"))
		
		self.get_node("Arrow").set_hidden(false)
		Globals.set("LEVEL_CLEAR", true)
		get_tree().set_pause(true)
	
	self.set_text(str(obj_done) + "/" + str(obj_max))
	self.update()