extends Area2D

export(Vector2) var pbar_offset

const pbar_rect = Vector2(8, 32)
export(Color) var pbar_bgc = Color("222222")

export(Color) var pbar_defc = Color("C3AE55")
export(Color) var pbar_winc = Color("FFFF00")
const pbar_flick_speed = 2
var pbar_fgc = pbar_defc
var pbar_flick_t = 0.0
var pbar_dir = 0

export(float) var pee_power = 1
export(float) var pee_decay = 0.3
var player
var pee_progress = 0.0
var done = false

func _ready():
	get_node("../ObjectiveLabel").add_to_max()
	
	self.connect("body_enter", self, "on_enter")
	self.connect("body_exit", self, "on_exit")
	
	set_process(true)

func _process(delta):
	if(pee_progress >= 1.0):
		if(not done):
			done = true
			self.get_node("SamplePlayer").play("ok")
			
			if(self.has_node("Quit")):
				get_tree().quit()
			elif(self.has_node("Play")):
				get_node("/root/globals").switch_level(1)
			else:
				get_node("../ObjectiveLabel").add_to_objectives()
		
		if(pbar_flick_t <= 0.0):
			pbar_dir = 1
		elif(pbar_flick_t >= 1.0):
			pbar_dir = -1
		pbar_flick_t += pbar_dir * pbar_flick_speed * delta
		
		pbar_fgc = pbar_defc.linear_interpolate(pbar_winc, pbar_flick_t)
		#update()
		self.get_node("Sprite").set_frame(pee_progress * 9)
	else:
		if(player != null and player.get_parent().curr_state == player.get_parent().state.PEE):
			pee_progress += pee_power * delta
			#update()
			self.get_node("Sprite").set_frame(pee_progress * 9)
		elif(pee_progress > 0.0):
			pee_progress -= pee_decay * delta
			#update()
			self.get_node("Sprite").set_frame(pee_progress * 9)

#func _draw():
#	draw_rect(Rect2(pbar_offset, pbar_rect), pbar_bgc)
#	draw_rect(Rect2(pbar_offset, Vector2(pbar_rect.x, pbar_rect.y * pee_progress)), pbar_fgc)

func on_enter(body):
	player = body

func on_exit(body):
	player = null