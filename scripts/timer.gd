extends Timer

func _ready():
	self.connect("timeout", self, "on_timeout")
	self.connect("timeout", get_node("../Player"), "on_timeout")
	
	self.set_wait_time(80.0)
	self.start()
	set_process(true)

func _process(delta):
	var seconds = int(self.get_time_left()) % 60
	var sec_str = str(seconds)
	if(seconds < 10):
		sec_str = "0" + sec_str
	
	var minutes = int(int(self.get_time_left()) / 60)
	var min_str = str(minutes)
	if(minutes < 10):
		min_str = "0" + min_str
	
	self.get_node("Label").set_text(min_str + ":" + sec_str)
	self.get_node("Label").update()

func on_timeout():
	self.stop()