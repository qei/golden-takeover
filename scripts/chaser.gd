extends Sprite

export(float) var spawn_t = 0.0
export(float) var spawn_wait = 4.0
#export(Vector2) var spawn_pos
var spawn_pos = Vector2()

#const dying_dur = 1.2

const lin_speed = 100
const hit_speed = 60
const target_dist = 30
var velocity = Vector2(0, 0)

const def_health = 4
var health

#const recover_dur = 0.5
var recover_t = 0.0

#const attack_dur = 0.4
const attack_cool_off = 0.8
var attack_t = 0.0
var attack_landed = false

#const stance_dur = 0.6

enum state {WALK, ATTACK, HIT, STANCE, DYING, DEAD}
var curr_state

var player

func _ready():
	set_fixed_process(true)
	set_process(true)
	
	spawn_pos = self.get_global_pos()
	curr_state = state.DEAD

func reset():
	self.set_global_pos(spawn_pos)
	health = def_health
	spawn_t = 0.0
	attack_t = 0.0
	attack_landed = false
	velocity = Vector2(0, 0)
	curr_state = state.WALK
	self.get_node("AnimationPlayer").play("walk")
	set_hidden(false)

func _process(delta):
	if(curr_state == state.HIT or curr_state == state.DYING):
		if(self.get_node("AnimationPlayer").get_current_animation() != "hit"):
			self.get_node("AnimationPlayer").stop()
			self.get_node("AnimationPlayer").play("hit")
	
	elif(curr_state == state.ATTACK):
		if(self.get_node("AnimationPlayer").get_current_animation() != "attack"):
			self.get_node("AnimationPlayer").stop()
			self.get_node("AnimationPlayer").play("attack")
	
	elif(curr_state == state.WALK):
		if(self.get_node("AnimationPlayer").get_current_animation() != "walk"):
			self.get_node("AnimationPlayer").stop()
			self.get_node("AnimationPlayer").play("walk")
			
	elif(curr_state == state.STANCE):
		if(self.get_node("AnimationPlayer").get_current_animation() != "stance"):
			self.get_node("AnimationPlayer").stop()
			self.get_node("AnimationPlayer").play("stance")
	
func _fixed_process(delta):
	if(curr_state == state.DEAD):
		spawn_t += delta
		if(spawn_t >= spawn_wait and not Globals.get("LEVEL_CLEAR")):
			reset()
	elif(curr_state == state.DYING):
		spawn_t += delta
		
		self.set_hidden(int(spawn_t * 10 / 3 + 1) % 2)
		
		if(spawn_t >= self.get_node("AnimationPlayer").get_current_animation_length()):
			spawn_t = 0.0
			
			self.set_pos(spawn_pos)
			self.set_hidden(true)
			curr_state = state.DEAD
	else:
		if(curr_state == state.ATTACK or attack_t > 0.0 and curr_state != state.STANCE):
			attack_t += delta
			if(attack_t >= attack_cool_off):
				attack_t = 0.0
		
		if(curr_state == state.HIT):
			recover_t += delta
			if(recover_t >= self.get_node("AnimationPlayer").get_current_animation_length()):
				recover_t = 0.0
				curr_state = state.WALK
			
			self.set_global_pos(self.get_global_pos() + velocity * delta)
		
		elif(curr_state == state.STANCE):
			attack_t += delta
			if(attack_t >= self.get_node("AnimationPlayer").get_current_animation_length()):
				attack_t = 0.0
				curr_state = state.ATTACK
			if(player == null):
				attack_t = 0.0
				curr_state = state.WALK
				attack_landed = false
		
		elif(curr_state == state.ATTACK):
			if(attack_t >= self.get_node("AnimationPlayer").get_current_animation_length()):
				curr_state = state.WALK
				attack_landed = false
			
			if(player != null and attack_t < self.get_node("AnimationPlayer").get_current_animation_length() and not attack_landed):
				player.get_parent().hit(self.get_global_pos())
				attack_landed = true
		
		else:
			var dir = get_node("../Player").get_global_pos() - self.get_global_pos()
			
			if(dir.length() >= target_dist):
				curr_state = state.WALK
				velocity = dir.normalized() * lin_speed
				
				self.set_global_pos(self.get_global_pos() + velocity * delta)
			
			elif(player != null and curr_state != state.ATTACK):
				curr_state = state.STANCE
			
			if(velocity.x < 0):
				self.set_scale(Vector2(-1, 1))
			else:
				self.set_scale(Vector2(1, 1))

func hit(attack_dir):
	if(curr_state != state.HIT and curr_state != state.DEAD and curr_state != state.DYING):
		self.get_node("SamplePlayer").play("hit_2")
		health -= 1
		if(health <= 0):
			curr_state = state.DYING
		else:
			curr_state = state.HIT
			attack_t = 0.0
			attack_landed = false
			
			var dir = self.get_global_pos() - attack_dir
			velocity = dir.normalized() * hit_speed

func on_enemy_enter(body):
	#print("player enter")
	player = body

func on_enemy_exit(body):
	#print("player exit")
	player = null